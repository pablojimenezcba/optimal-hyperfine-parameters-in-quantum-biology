import numpy as np
from datetime import datetime
from dataclasses import dataclass
from quantum.quantum_operations import project_ket,expect_value
from optimization.metrics import fraction_yield,quantum_yield,integrated_yield,ensemble_yields

@dataclass
class GPM:
    H : object
    ode : object
    ensemble : list
    
    def gradient_hyperfine(self,lambda_):
        d_A = np.zeros(self.H.A.shape)
        for i in range(self.H.n_nuclei):
            tiknov = lambda_*self.H.A[:,i].sum(axis=(1,2))
            d_A[:,i,0,0] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%ix'%(i+1)].dot(self.H.op.op['S_1x']),self.ode.y_t).imag
            d_A[:,i,1,1] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iy'%(i+1)].dot(self.H.op.op['S_1y']),self.ode.y_t).imag
            d_A[:,i,2,2] = tiknov +2* expect_value(self.ode.w_t,self.H.op.op['I_%iz'%(i+1)].dot(self.H.op.op['S_1z']),self.ode.y_t).imag
        return d_A
    
    def hyperfine_optimization(self,lr,lambda_,epochs,tol,update_lr=False,bounds=((-100,100)),maximize=False):
        J = np.zeros((epochs+1)) #include initial yield
        g_A = np.zeros(self.H.A.shape)
        m,M = bounds
                
        for i in range(epochs):

            #update learning rate if needed
            if update_lr and i and not i%update_lr:
                lr_A = self.update_lr(lambda_,bounds)
            
            #reset gradient
            g_A *= 0

            #solve ode for each initial condition
            for j,psi_0 in enumerate(self.ensemble):
                self.ode.set_y(psi_0)
                self.ode.solve_forward('rk4')
                self.ode.solve_backward('rk4')
                #calculate gradient
                g_A += self.gradient_hyperfine(lambda_)/self.H.Z
                J[i] += quantum_yield(self.H,self.ode.y_t)/self.H.Z
            #gradient descent
            self.H.A -= (-1)**(maximize)*lr*g_A
            #restrict hyperfine to bounds
            self.H.A =np.clip(self.H.A,m,M)
            #break if converged
            if i>0 and abs(J[i]-J[i-1])<tol:
                break
            #print progress
            if i%10==0:
                print('Epoch: %i/%i, J: %.8e'%(i,epochs,J[i]))
            else:
                print('Epoch: %i/%i, J: %.8e'%(i,epochs,J[i]),end='\r')

        #calculate final yield
        f_y,i_y,J[i+1] = ensemble_yields(self.ensemble,self.ode,self.H,method='rk4')
        return J[:i+2],f_y,i_y,g_A

    def update_lr(self,lambda_,bounds):
        print('Starting lr search\n')
        A = self.H.A.copy()
        lrs = np.logspace(-3,-1,7)
        lr = np.argmax( [ #min if minimization
            self.hyperfine_optimization(l,lambda_,2,0,bounds=bounds)[0][-1]
            for l in lrs
        ])
        lr=lrs[lr]
        self.H.A = A
        print('lr search ended\n')
        return lr