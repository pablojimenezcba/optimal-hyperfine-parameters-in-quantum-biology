import numpy as np
from scipy.sparse import csc_matrix

def tridiag(a, b, c, k1=-1, k2=0, k3=1):
	return np.diag(a, k1) + np.diag(b, k2) + np.diag(c, k3)

def spinOperatorX(g_I,sparse=True):
	I = 0.5*(g_I-1)
	Ms = np.arange(I,-I,-1)
	band = 0.5*np.sqrt(I*(I+1) - Ms*(Ms-1))
	I_x = tridiag(band,np.zeros((1,g_I)),band)
	if sparse:
		return csc_matrix(I_x)
	return I_x

def spinOperatorY(g_I,sparse=True):
	I = 0.5*(g_I-1)
	Ms = np.arange(I,-I,-1)
	band = 0.5j*np.sqrt(I*(I+1) - Ms*(Ms-1))
	I_y = tridiag(band,np.zeros((1,g_I)),-band)
	if sparse:
		return csc_matrix(I_y)
	return I_y

def spinOperatorZ(g_I,sparse=True):
	I = 0.5*(g_I-1)
	Ms = np.arange(I,-I-1,-1).T
	I_z = np.diag(Ms)
	if sparse:
		return csc_matrix(I_z)
	return I_z