import numpy as np

def norm(y):
	if len(y.shape)==2:
		return np.einsum('ti,ti->t',y.conj(),y)
	if len(y.shape)==1:
		y.conj().dot(y.T)
	assert(0)

def bra_ket(y1,y2):
	if len(y1.shape) == len(y2.shape) == 2:
		return np.einsum('ti,ti->t', y1.conj(), y2)
	if len(y1.shape) == len(y2.shape) == 1:
		y1.conj().dot(y2.T)
	assert(0)

def project_ket(O,y):
	if len(O.shape) == 3 and len(y.shape) == 2:
		return np.einsum('tij,tj->ti', O, y)
	if len(O.shape) == 2 and len(y.shape) == 2:
		return np.einsum('ij,tj->ti', O, y)
	if len(O.shape) == 3 and len(y.shape) == 1:
		return np.einsum('tij,j->ti', O, y)
	if len(O.shape) == 2 and len(y.shape) == 1:
		return O.dot(y)
	if len(O.shape) == 3 and len(y.shape) == 2:
		return np.einsum('tij,tj->ti', O, y)
	assert(0)

def expect_value(y1,O,y2):
	return bra_ket(y1,project_ket(O,y2))
