import numpy as np
import mpmath
from scipy.linalg import expm
import sys

def runge_kutta_4(f,t,h,y_0):
    k1 = f(t,y_0)
    k2 = f(t+h/2,y_0+h*k1/2)
    k3 = f(t+h/2,y_0+h*k2/2)
    k4 = f(t+h,y_0+h*k3)
    return y_0 + h*(k1+2*k2+2*k3+k4)/6


def rk4(f,t,h,y_0):
    y = np.zeros((len(t),*y_0.shape)).astype(complex)
    if h>0:
        y[0] = y_0
        for i in range(1,len(t)):
            y[i] = runge_kutta_4(f,t[i-1],h,y[i-1])
    else:
        y[-1] = y_0
        for i in range(len(t)-2,-1,-1):
            y[i] = runge_kutta_4(f,t[i+1],h,y[i+1])
    return y


def propagateExpM(P,Q,y_0,dt):
    y_0 = np.array([mpmath.mpc(x_elem.real, x_elem.imag) for x_elem in y_0.flatten()]).reshape(y_0.shape)
    return expm(dt*P).dot(y_0)+Q*dt

def Linear_ODE(P,Q,t,h,y_0):
    y = np.zeros((len(t),y_0.shape[0])).astype(complex)
    if h>0:
        y[0] = y_0
        for i in range(1,len(t)):
            y[i] = propagateExpM(P(i-1),Q(i-1),y[i-1],h)
    else:
        y[-1] = y_0
        for i in range(len(t)-2,-1,-1):
            y[i] = propagateExpM(P(i+1),Q(i+1),y[i+1],h)
    return y