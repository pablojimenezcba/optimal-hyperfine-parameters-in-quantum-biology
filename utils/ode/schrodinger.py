import numpy as np
from quantum.quantum_operations import project_ket
from ode.methods import rk4,Linear_ODE
from scipy.sparse import csc_matrix

#Uncomment if working with sparse matrices
#propagateExpM_ = lambda P,Q,y_0,dt: propagateExpM(P,Q,y_0,dt,exponential=True,sparse=False)

class H_ODE:
    def __init__(self,H,y_0=None):
        self.H = H
        if y_0 is not None:
            self.set_y(y_0)
    def set_y(self,y_0):
        self.y_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)
        self.y_t[0] = y_0
        self.w_t = np.zeros((self.H.n_time, y_0.shape[0])).astype(complex)

    def solve_forward(self,method,build_Ht=True):
        if build_Ht:
            #Calculates H for all times before solving
            self.H.build_H_t()

        Aux = lambda idx,y,b: (-1j)*self.H.H_0[idx].dot(y) if b else (-1j)*self.H(idx).dot(y)
        P = lambda idx,y: Aux(idx,y,b=build_Ht)
        Q = lambda idx : 0

        if method=='rk4':
            f = lambda t,y : self.f(t,y,P,Q)
            self.y_t = rk4(f,self.H.t,self.H.dt,self.y_t[0])
        elif method=='Ht':
            P = lambda idx: Aux(idx,np.eye(self.y_t.shape[1]),b=build_Ht)
            Q = lambda idx: 0
            self.y_t = Linear_ODE(P,Q,self.H.t,self.H.dt,self.y_t[0])
        else:
            raise NotImplementedError
    
    def solve_backward(self,method,built_Ht=True):
        Aux = lambda idx,y,b: (-1j)*(self.H.H_0[idx]+ 2.0j*self.H.K).dot(y) if b else (-1j)*(self.H(idx).conj()).dot(y)
        P = lambda idx,y: Aux(idx,y,built_Ht)
        Q = lambda idx : -0.5*self.H.k_S * project_ket(self.H.O,self.y_t[idx])
        if method=='rk4':
            f = lambda t,y : self.f(t,y,P,Q)
            self.w_t = rk4(f,self.H.t,-self.H.dt,self.w_t[-1])
        elif method=='Ht':
            P = lambda idx: Aux(idx,np.eye(self.y_t.shape[1]),built_Ht)
            self.w_t = Linear_ODE(P,Q,self.H.t,-self.H.dt,self.w_t[-1])
        else:
            raise NotImplementedError
            
    def f(self,t,y,P,Q):
        idx = (t/self.H.dt+1.e-6)
        if abs(idx-int(idx))<2.e-6:
            return P(int(idx),y) + Q(int(idx))
        d = idx-int(idx)
        p1 = P(int(idx),y) + Q(int(idx))
        p2 = P(int(idx+1),y) + Q(int(idx+1))
        return (p1+p2)/2